#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/env.sh

if [[ "${1}" = 'periodic-backup' ]]; then
  echo "${BACKUP_CRON_EXPRESSION} backup" | crontab - && crond -f -L /dev/stdout
else
  exec "${@}"
fi