#!/bin/bash

source env.sh

function info {
    bold="\033[1m"
    reset="\033[0m"
    echo -e "$bold[INFO] $1$reset"
}

function check_for_empty_source {
    if [[ -z "$(ls -A ${SOURCE})" ]]; then
      echo "source folder is empty, no backup needed..."
      exit 1
    fi
}
