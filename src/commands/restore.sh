#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh
source $(dirname $0)/../docker.sh

stop_labelled_containers ${CONTAINER_RESTORE_STOP_LABEL}
info "starting restore command..."
eval duplicity restore ${DUPLICITY_RESTORE_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET} ${SOURCE}
start_labelled_containers ${CONTAINER_RESTORE_STOP_LABEL}
exec_labelled_containers_scripts ${CONTAINER_RESTORE_POST_SCRIPT_LABEL}
info "done."