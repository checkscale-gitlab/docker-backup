#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh

info "starting collection-status command..."
eval duplicity collection-status ${DUPLICITY_COLLECTION_STATUS_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET}
info "done."