#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh

info "starting cleanup command..."
eval duplicity cleanup ${DUPLICITY_CLEANUP_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET}
info "done."